<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'db_quiz' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '5PD 2)iIh7Fi^M0xAdRnf.Y>{n,XAf@kj<+T*e.N|oAH@QRbBx&e|2Y=5}4}J:c/' );
define( 'SECURE_AUTH_KEY',  'm/`s06X>tG0)bZ&EyxkE/y*UW#CZK$?}!rJZ{xy|KAFHhM6m<I3~{l_:Ih;t&fhj' );
define( 'LOGGED_IN_KEY',    'yl>g!h/0Mv]j1JkH9]Yz<W&~&vAvZIlStrgH#]*oj~XK6d&.(Tm*tMl}%Z`bp(ra' );
define( 'NONCE_KEY',        'VQZN/^UKqsmQr1p(i y*uV+Z4*RLd_jRE#px%`>PM4YFxX|$F%b%fA{$=QlwaLev' );
define( 'AUTH_SALT',        'D]luW4%bqSZ#q;ez[i9V*<nh(e+oA:R0Zfm2UL2yxe#,20-~~.7Qk0gE!N}?YMXO' );
define( 'SECURE_AUTH_SALT', ':-DOq7)Zx$:wMpcaxq1?Nxs.VC%jOcK2Zr0ZIFUESyDaY$ax=BVQOuML4K!`F }J' );
define( 'LOGGED_IN_SALT',   'LwVtTh=3.5`[rsBZ$u2ER1W7b:}yz = &]t1)TyvXxCG&8uWDpUA8UuHHK+$gfs+' );
define( 'NONCE_SALT',       '^+jf5at(C=jIbJxryc[HF4DFO] _6qz1*.lOYY4&$y}@i13)1.#+.[X{+D(_wlrL' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );
define('WP_ALLOW_MULTISITE', true);
/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
